import {Component, OnInit, TemplateRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser } from '../model/User';
import { Status } from '../model/User';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import { AdminOfficeService } from '../service.project/admine.ofice.service';
@Component({
    moduleId: module.id,
    selector: 'admin-component',
    templateUrl: 'admin.component.html',
    providers: []
})
export class AdminComponent implements OnInit {

    public localState: any;
    public searchText;
    public status: any = Status;
    public modalRef: BsModalRef;
    public dataList: any;
    public selectUser: IUser;
    constructor(
        public route: ActivatedRoute,
        private router: Router,
        private modalService: BsModalService,
        private adminOfficeService: AdminOfficeService
    ) {
        this.getUsers();
    }

    public ngOnInit() {
        this.route
            .data
            .subscribe((data: any) => {

                this.localState = data.yourData;
            });

        console.log('hello `Registratration` component');

        this.asyncDataWithWebpack();
    }
    private asyncDataWithWebpack() {
        setTimeout(() => {

            System.import('../../assets/mock-data/mock-data.json')
                .then((json) => {
                    console.log('async mockData', json);
                    this.localState = json;
                });

        });
    }

    public openModal(template: TemplateRef<any>, user) {
        this.selectUser = user;
        this.modalRef = this.modalService.show(template);
    }

    public closeModal() {
        this.modalRef.hide();
        this.modalRef = null;
    }

    public getUsers() {
        this.adminOfficeService.getUsers().subscribe(
            data=> {
                this.dataList = data.data;
            },
            err=> {
                this.router.navigate(['/auth']);
            }
        )
    }

    public getNotyfay($event) {
        this.closeModal();
    }

    public getNewUser($event) {
        this.getUsers();
        this.closeModal();
    }

}

