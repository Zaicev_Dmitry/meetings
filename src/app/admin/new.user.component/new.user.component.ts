import {Component, OnInit, Output, EventEmitter, TemplateRef, Input} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from "rxjs/Observable";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
    emailErrorMessages, returnNameErrorMessages,
    returnPasswordErrorMessages
} from '../../utils/validation/validation.messages.util';
import { EMAIL_REGEXP } from  '../../utils/validation/validation.regexp.util';
import { passwordErrorMessages } from '../../utils/validation/validation.messages.util';
import {AbstractFormComponent} from "../../abstract.classes/abstract-form-component";
import {repeatPasswordValidator} from "../../utils/validation/validation.function";
import {AdminOfficeService} from "../../service.project/admine.ofice.service/admine.ofice.service";

@Component({
    moduleId: module.id,
    selector: 'new-user',
    templateUrl: 'new.user.component.html',
    providers: []
})

export class NewUserComponent extends AbstractFormComponent implements OnInit {
    public modalRef: BsModalRef;
    public model = <IRegistration>{};

    @Output() _notifyCancel: EventEmitter<any> = new EventEmitter();
    @Output() _notifyCreateUser: EventEmitter<any> = new EventEmitter();
    constructor( public modalService: BsModalService, private formBuilder: FormBuilder, public adminOfficeService: AdminOfficeService ) {
        super();
        /*this.meetingObjectSend.subscribe(() => {
         console.log(this.meetingObjectSend);
         })*/
    }

    public ngOnInit() {
        super.ngOnInit();
        console.log(this.formBuilder);
    }

    public setCancel(e) {
        this._notifyCancel.emit(e);
    }

    protected buildForm(): FormGroup {
        let form = this.formBuilder.group({
            first_name: ['', {updateOn: 'blur', validators: []}],
            last_name: ['', {updateOn: 'blur', validators: []}],
            email: ['', { updateOn: 'blur', validators: [Validators.required, Validators.minLength(2), Validators.maxLength(255), Validators.pattern(EMAIL_REGEXP)]}],
            organization: ['', {updateOn: 'blur', validators: []}],
            password: ['', {updateOn: 'blur', validators: [Validators.required, Validators.minLength(12), Validators.maxLength(255)]}],
            returnpassword: ['', {updateOn: 'blur', validators: [Validators.required, Validators.minLength(12), Validators.maxLength(255), repeatPasswordValidator(this)]}],
        });
        return form;
    }

    protected submit(): void {
        Object.assign(this.model, this.form.value);
        this.adminOfficeService.createUser(this.model).subscribe(
            data=> {
                this._notifyCreateUser.emit(this.model);
            },
            error => {

            }
        )

    }


    protected getValidationMessages(): {} {
        return ({
            email: emailErrorMessages,
            password: passwordErrorMessages,
            organization: returnNameErrorMessages,
            first_name: returnNameErrorMessages,
            last_name: returnNameErrorMessages,
            returnpassword: returnPasswordErrorMessages
        })
    }

}

export interface IRegistration {
    first_name: string,
    last_name: string,
    email: string,
    organization: string,
    password: string
}