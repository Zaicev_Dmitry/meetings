import { AdminComponent } from './admin.component';
import {NewUserComponent} from "./new.user.component/new.user.component";
import {CREATE_COMPONENTS} from "./create.user/index";

export const ADMIN_COMPONENTS: any[] = [
    AdminComponent,
    CREATE_COMPONENTS,
    NewUserComponent,
];