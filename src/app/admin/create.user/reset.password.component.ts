import {Component, OnInit, Output, EventEmitter, TemplateRef, Input} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from "rxjs/Observable";

@Component({
    moduleId: module.id,
    selector: 'reset-password-admin',
    templateUrl: 'create.user.template.html',
    providers: []
})

export class ResetPasswordComponent implements OnInit {
    public modalRef: BsModalRef;
    public title: any = "to reset someone's password";
    public value: any = "reset";
    @Output() _notifyCancel: EventEmitter<any> = new EventEmitter();
    constructor( public modalService: BsModalService ) {
        /*this.meetingObjectSend.subscribe(() => {
         console.log(this.meetingObjectSend);
         })*/
    }

    public ngOnInit() {

    }
    public setCancel(e) {
        this._notifyCancel.emit(e);
    }

    public setActivated(e) {

    }

}
