import {Component, OnInit, Output, EventEmitter, TemplateRef, Input} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from "rxjs/Observable";
import { AdminOfficeService } from '../../service.project/admine.ofice.service';
import {IUser, Status} from "../../model/User";

@Component({
    moduleId: module.id,
    selector: 'verify-user',
    templateUrl: 'create.user.template.html',
    providers: []
})

export class VerifyUserComponent implements OnInit {
    public modalRef: BsModalRef;
    public title: any = "to verify a user";
    public value: any = "verify";
    @Output() _notifyCancel: EventEmitter<any> = new EventEmitter();
    @Input() user: IUser;
    constructor( public modalService: BsModalService, public adminOfficeService: AdminOfficeService) {
        /*this.meetingObjectSend.subscribe(() => {
         console.log(this.meetingObjectSend);
         })*/
        console.log(this.user);
    }

    public ngOnInit() {
    }

    public setCancel(e) {
        this._notifyCancel.emit(e);
    }

    public setActivated(e) {
        console.log(this.user);
        if(this.user) {
            this.adminOfficeService.verifyUser(this.user.id).subscribe(
                data=> {
                    this.user.status = Status.ACTIVE;
                    this._notifyCancel.emit(e);
                },
                error=> {

                }
            )
        }
    }

}