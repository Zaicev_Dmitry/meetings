import {DeleteUserComponent} from "./delete.user.component";
import {ResetPasswordComponent} from "./reset.password.component";
import {VerifyUserComponent} from "./verify.user.component";

export const CREATE_COMPONENTS: any[] = [
    DeleteUserComponent,
    ResetPasswordComponent,
    VerifyUserComponent
];

