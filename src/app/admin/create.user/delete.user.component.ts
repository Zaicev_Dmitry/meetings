import {Component, OnInit, Output, EventEmitter, TemplateRef, Input} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from "rxjs/Observable";
import {AdminOfficeService} from "../../service.project/admine.ofice.service/admine.ofice.service";
import {IUser, Status} from "../../model/User";

@Component({
    moduleId: module.id,
    selector: 'delete-user',
    templateUrl: 'create.user.template.html',
    providers: []
})

export class DeleteUserComponent implements OnInit {
    public modalRef: BsModalRef;
    public title: any = "to delete a user";
    public value: any = "delete";
    @Output() _notifyCancel: EventEmitter<any> = new EventEmitter();
    @Input() user: IUser;
    constructor( public modalService: BsModalService, public adminOfficeService: AdminOfficeService ) {
        /*this.meetingObjectSend.subscribe(() => {
         console.log(this.meetingObjectSend);
         })*/
    }

    public ngOnInit() {

    }

    public setCancel(e) {
        this._notifyCancel.emit(e);
    }

    public setActivated(e) {
        if(this.user) {
            this.adminOfficeService.deleteUser(this.user.id).subscribe(
                data=> {
                    this._notifyCancel.emit(e);
                },
                error=> {
                    this.user.status = Status.DELETED;
                    this._notifyCancel.emit(e);
                }
            )
        }
    }

}
