import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { JwtModule } from '@auth0/angular-jwt';
import { AUTHITIFICATION } from '../utils/api/api';
import { LOGOUT } from '../utils/api/api';
import { IUser } from "../model/User";
import { GET_USER_AUTH } from '../utils/api/api';

@Injectable()
export class AuthService {

   constructor(private http: HttpClient) {
   };

   public login(username, password) {
       return this.http.post<any>(AUTHITIFICATION, { email: username, password: password })
           .map(res  => {
               console.log(res);
               if (res.access_token  && res .token_type) {
                   localStorage.setItem('currentUser', JSON.stringify(res.access_token));
               }
               return res ;
           });
   }

   public logout() {
       //localStorage.removeItem('currentUser');
       return this.http.delete(LOGOUT);
   }

   public isAuthenticated(): boolean {
       return localStorage.getItem('currentUser') ? true : false;
       //return !this.jwtModule.isTokenExpired(token);
    }

    public getUsers(): Observable<any> {
        return this.http.post<IUser[]>(GET_USER_AUTH, null);
    }

}