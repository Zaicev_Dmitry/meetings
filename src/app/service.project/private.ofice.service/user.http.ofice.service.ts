import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { TEAM_MEMBERS } from '../../utils/api/api'

@Injectable()

export class UserHttpOffice {
    constructor(private http: HttpClient) {

    }

    public getTeam() {
        return this.http.get<any>(TEAM_MEMBERS);
    }

    public getSchedule() {

    }

    public getUser() {

    }

    public setUser(email) {
        //let body = new HttpParams();
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
      //  body.append('name', email);
        return this.http.post(TEAM_MEMBERS, {'email': email});
    }

}