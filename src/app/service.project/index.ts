import { AuthService } from './auth.service';
import { AuthGuardService } from './auth.guard.service'
import { UserHttpOffice } from "./private.ofice.service/user.http.ofice.service";
import { AdminOfficeService } from './admine.ofice.service'
export const SERVICE_PROJECT = [
    AuthService,
    AuthGuardService,
    UserHttpOffice,
    AdminOfficeService
];
