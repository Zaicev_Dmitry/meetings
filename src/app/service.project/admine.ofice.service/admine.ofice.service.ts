import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { ADMIN_USERS } from '../../utils/api/api';
import { Observable } from "rxjs/Observable";
import { IUser } from "../../model/User";
import {IRegistration} from "../../admin/new.user.component/new.user.component";

@Injectable()

export class AdminOfficeService {

    constructor(private http: HttpClient) {

    }

    public getUsers(): Observable<any> {
        return this.http.get<IUser[]>(ADMIN_USERS);
    }

    public deleteUser(id) {
        let delete_url = ADMIN_USERS+'/'+id;
        return this.http.delete(delete_url);
    }

    public verifyUser(id) {
        let verify_url = ADMIN_USERS+'/'+id+'/verify';
        return this.http.post(verify_url, {'id': id});
    }

    public createUser(form: IRegistration) {
        delete form['returnpassword'];
        return this.http.post(ADMIN_USERS, form );
    }

}
