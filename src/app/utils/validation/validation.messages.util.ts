export let emailErrorMessages = {
    required: 'This field is required.',
    pattern: 'Invalid format email.'
};

export let passwordErrorMessages = {
    required: 'This field is required.',
    minlength: 'Short password. Min length password 6 is symbols'
};

export let returnPasswordErrorMessages = {
    required: 'This field is required.',
    pattern: 'Password mismatch'
};

export let returnNameErrorMessages = {
    required: 'This field is required.',
};
