import {AbstractControl, ValidatorFn} from '@angular/forms';

export function repeatPasswordValidator(object): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const value = control.value;
        let password;
        try {
            password = object.form['controls']['password'].value;
        } catch (err) {
        }
        return password != value ? {'pattern': {value}} : null;
    };
}