export class DevelopUtil {

    private static HOST = '';

    public static getHost() {
        return DevelopUtil.HOST;
    }

    public static url(url: string) {
        return DevelopUtil.HOST + url;
    }
}
