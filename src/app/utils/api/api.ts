import { DevelopUtil } from '../develop.utils';
export const SENT_SCHEDULE = DevelopUtil.getHost() + 'schedule';
export const SCHEDULE_SLOT = DevelopUtil.getHost() + 'schedule-slot';
export const GET_SCHEDULE = DevelopUtil.getHost() + 'meetings';
export const AUTHITIFICATION = DevelopUtil.getHost() + 'api/auth/login';
export const GET_USER_AUTH = DevelopUtil.getHost() + 'api/auth/user';
export const LOGOUT = DevelopUtil.getHost() + 'api/auth/access';
export const REGISTRATION = DevelopUtil.getHost() + 'auth/register';
export const CHANGE_PASSWORD = DevelopUtil.getHost() + 'auth/user/change-password';
export const USERS = DevelopUtil.getHost() + 'users';
export const TEAM_MEMBERS = DevelopUtil.getHost() + 'api/members';

export const ADMIN_USERS = DevelopUtil.getHost() + 'api/admin/users';