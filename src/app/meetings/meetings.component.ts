import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'meetings',
    templateUrl: 'meetings.component.html',
    providers: []
})
export class MeetingsComponent implements OnInit {

    public localState: any;
    public _meetingList: any[] = [
        {
            title: 'Meeting with Elon',
            time: '18:00',
            location: 'SpaceX HQ',
            participants: 'Cheese and Tomato'
        },
        {
            title: 'Meeting with Margot',
            time: '20:00',
            location: 'Hollywood',
            participants: 'Cheese, Tomato and Onions'
        },
        {
            title: 'Meeting with Mac',
            time: '00:00',
            location: 'Brazzers',
            participants: 'Cheese, Tomato, Onions, Potatoes and Cucumber'
        },
    ];
    constructor(
        public route: ActivatedRoute
    ) {}

    public ngOnInit() {
        this.route
            .data
            .subscribe((data: any) => {

                this.localState = data.yourData;
            });

        console.log('hello `Registratration` component');

        this.asyncDataWithWebpack();
    }
    private asyncDataWithWebpack() {
        setTimeout(() => {

            System.import('../../assets/mock-data/mock-data.json')
                .then((json) => {
                    console.log('async mockData', json);
                    this.localState = json;
                });

        });
    }

    public deleteMeeting(index): void {
        this._meetingList.splice(index, 1);
    }
}
