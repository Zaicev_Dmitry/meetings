import { HeaderComponent } from './header/header.component';
import  { HomeComponent } from './home.component';
import  { HowWorkComponent } from './howwork/howwork.component';
import { HowmuchComponent } from './howmuch/howmuch.component';
import { CoruselComponent } from './courusel/courusel.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { StartSchedulingComponent } from './start/start.component';

export const HOME_COMPONENTS: any[] = [
    HomeComponent,
    HeaderComponent,
    HowWorkComponent,
    HowmuchComponent,
    CoruselComponent,
    CalculatorComponent,
    StartSchedulingComponent
];

export * from './home.component';
export  * from './header/header.component';
export * from './howwork/howwork.component';
export * from './howmuch/howmuch.component';
export * from './courusel/courusel.component';
export * from './calculator/calculator.component';
export * from './start/start.component'
