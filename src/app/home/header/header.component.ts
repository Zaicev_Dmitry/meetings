import { Component, Input, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'header.component.html',
    selector: 'fm-home-header',
    providers: []
})

export class HeaderComponent implements OnInit {
    @Input()
    public data: any;

    constructor() {
    }

    public ngOnInit(): void {
        console.log(this.data);
    }
}