import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'start.component.html',
    selector: 'fm-home-start',
    providers: []
})

export class StartSchedulingComponent implements OnInit {

    constructor() {
    }

    public ngOnInit(): void {
        console.log('start');
    }

}