import { Component, Input, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'howmuch.component.html',
    selector: 'fm-how-much',
    providers: []
})

export class HowmuchComponent implements OnInit {
    @Input()
    public data: any;

    constructor() {
    }

    public ngOnInit(): void {
        console.log(this.data);
    }
}