import { Component, Input, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'howwork.component.html',
    selector: 'fm-how-work',
    providers: []
})

export class HowWorkComponent implements OnInit {
    @Input()
    public data: any;

    constructor() {
    }

    public ngOnInit(): void {
        console.log(this.data);
    }
}