import { Component, Input, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'courusel.component.html',
    selector: 'fm-home-corusel',
    providers: []
})

export class CoruselComponent implements OnInit {
    @Input()
    public data: any;

    constructor() {
        console.log(this.data);
    }

    public ngOnInit(): void {
        console.log(this.data);
        console.log('corusel');
    }

}