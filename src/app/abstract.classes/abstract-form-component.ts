import { OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
export abstract class AbstractFormComponent implements OnInit {

    form: FormGroup;
    formErrors: {};
    protected validationMessages = {};

    constructor() {
        this.formErrors = {};
    }

    public ngOnInit(): void {
        this.validationMessages = this.getValidationMessages();
        this.form = this.buildForm();
        this.subscribeValueChanges();

    };

    /**
     * Build reactive form method
     */
    protected abstract buildForm(): FormGroup;

    /**
     * Determine validation messages
     */
    protected abstract getValidationMessages(): {};

    /**
     * Method is called when the form is submitted
     */
    protected abstract submit(): void;

    /**
     * Listener of click on submit button
     * @param $event
     */
    protected onSubmit($event): void {
        if (this.form.invalid) {
            //if form invalid preventing next step trigger
            $event.preventDefault();
        }

        //when submit form without any changes on ui
        this.onValueChanged(null, true);

        if (this.form.valid) {
            this.submit();
        }
    }

    protected validateField(key: string, errorMessage: {}) {
        let control = this.form.controls[key];

        if (!control) {
            return;
        }

        if (control.valid) {
            this.formErrors[key] = '';
            return;
        }
        for (const errorKey in control.errors) {
            this.formErrors[key] = errorMessage.hasOwnProperty(errorKey) ? errorMessage[errorKey] : '';
        }
    }

    /**
     * Subscribe to changes on form
     */
    protected subscribeValueChanges(): void {
        this.form.valueChanges.subscribe((data) => this.onValueChanged(data));
    }

    /**
     * Handler of changes on form and fill formErrors property
     * @param data
     * @param {Boolean} force
     */
    onValueChanged(data?: any, force = false) {
        if (!this.form) {
            return;
        }
        this.checkErrors(this.form, force);
    }

    /**
     * Recursive function for collecting errors from FormControls
     * @param {{}} formGroup current form group
     * @param {Boolean} force force check on errors
     * @param {String} groupKey parent group key
     */
    private checkErrors(formGroup: any, force: boolean, groupKey?: string): void {
        for (let controlKey in formGroup.controls) {
            let combinationKey = groupKey ? groupKey + '.' + controlKey : controlKey;
            //this.formErrors[combinationKey] = '';
            const control = formGroup.get(controlKey);
            if (control instanceof FormGroup) {
                this.checkErrors(control, force, combinationKey);
            } else if (control instanceof FormArray) {
                for (let i = 0; i < control.controls.length; i++) {
                    let combinationKey2 = combinationKey + '.' + i;
                    this.checkErrors(control.controls[i], force, combinationKey2);
                }
            } else {
                if (control) {

                    const customValidationAction = control.blur;

                    if (force || (!customValidationAction && control.dirty && !control.valid)) {
                        let combinationKeyClean = combinationKey.replace(/[\.][0-9]+/g, '');
                        const messages = this.validationMessages[combinationKeyClean];
                        for (const errorKey in control.errors) {
                            this.formErrors[combinationKey] = messages && messages.hasOwnProperty(errorKey) ? messages[errorKey]: '';
                        }
                    }

                    //clean validation error messages
                    if (control.valid) {
                        this.formErrors[combinationKey] = '';
                    }

                }

            }
        }
    }

}
