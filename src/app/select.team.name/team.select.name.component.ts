import {Component, OnInit, EventEmitter, Output, SimpleChanges, Input} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
@Component({
    moduleId: module.id,
    selector: 'team-select-name',
    templateUrl: 'team.select.name.component.html',
    providers: []
})

export class TeamSelectNameComponent implements OnInit {
    public dataTeam = [];
    public teams = [];
    constructor( ) {
        let val = this;
        this.dataTeam = [{id: 1, name: 'team1'}, {id: 2, name: 'team2'}];
        this.dataTeam.forEach(function (team, index) {
            val.teams.push({value: team.id, viewValue: team.name});
        })
    }


    public ngOnInit() {

    }

}
