import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseModule, AlertModule, BsDatepickerModule , TimepickerModule } from 'ngx-bootstrap';
import { ModalModule, BsDropdownModule } from 'ngx-bootstrap';
import { CarouselModule } from 'ngx-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

/*
 * Platform and Environment providers/directives/pipes
 */
import { environment } from 'environments/environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HOME_COMPONENTS } from './home/index';
import { SelectTeamDirective } from './schedule/select.team/select.team.directive';
import { AboutComponent } from './about';
import { RegistrationComponent } from './registration';
import { ADMIN_COMPONENTS } from './admin'
import { AUTH_COMPONENT } from './authitification/';
import { SCHEDULE_COMPONENTS } from './schedule';
import { MeetingsComponent } from './meetings';
import { PROFILE_COMPONENTS } from './profile';
import { TEAM_COMPONENT } from './team';
import { NoContentComponent } from './no-content';
import { XLargeDirective } from './home/x-large';
import { DevModuleModule } from './+dev-module';
import { ScheduleHttpService } from './schedule/schedule.http.service';
import { DIRECTIVES } from './directive';
import { SERVICE_PROJECT } from './service.project';
import { PIPES } from './pipe'
import { TeamSelectNameComponent } from './select.team.name'
import '../styles/styles.scss';
import '../styles/headings.css';
import { JwtInterceptor } from "./service.project/interceptor";

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    AboutComponent,
    HOME_COMPONENTS,
    PROFILE_COMPONENTS,
    SCHEDULE_COMPONENTS,
    RegistrationComponent,
    TeamSelectNameComponent,
    TEAM_COMPONENT,
    AUTH_COMPONENT,
    ADMIN_COMPONENTS,
    SelectTeamDirective,
    MeetingsComponent,
    NoContentComponent,
    XLargeDirective,
    DIRECTIVES,
    PIPES,
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    BsDropdownModule.forRoot(),
    BsDatepickerModule .forRoot(),
    TimepickerModule.forRoot(),
    AlertModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    /**
     * This section will import the `DevModuleModule` only in certain build types.
     * When the module is not imported it will get tree shaked.
     * This is a simple example, a big app should probably implement some logic
     */
    ...environment.showDevModule ? [ DevModuleModule ] : [],
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    environment.ENV_PROVIDERS,
    APP_PROVIDERS,
    ScheduleHttpService,
    SERVICE_PROJECT,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ]
})
export class AppModule {}
