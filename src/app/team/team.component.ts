import {Component, OnInit, TemplateRef} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {UserHttpOffice} from "../service.project/private.ofice.service/user.http.ofice.service";

@Component({
    moduleId: module.id,
    selector: 'team',
    templateUrl: 'team.component.html',
    providers: []
})
export class TeamComponent implements OnInit {

    public localState: any;
    public modalRef: BsModalRef;
    public memberList: any[] = [
        {
            img: 'https://i.digiguide.tv/p/0809/tn-951-WillSmith-12203638190.jpg',
            name: 'Will Smith',
            email: 'will@example.com'
        },
        {
            img: 'http://www.kino-teatr.ru/news/5883/61931.jpg',
            name: 'Margot Robbie',
            email: 'margot@example.com'
        },
        {
            img: 'https://imgstorage2.contextly.com/thumbnails/inc42/7701253/150x150.jpg',
            name: 'Elon Musk',
            email: 'elon@example.com'
        },
        {
            img: 'https://scontent-sea1-1.cdninstagram.com/t51.2885-15/s480x480/e35/c0.134.1080.1080/25011967_1676179135773024_7908552809472065536_n.jpg?ig_cache_key=MTM3MjE0NTQzODYwMjYyOTE4Mg%3D%3D.2.c',
            name: 'Abigail Mac',
            email: 'abigail@example.com'
        }

    ];

    constructor(
        public route: ActivatedRoute,
        public modalService: BsModalService,
        public UserHttpOffice: UserHttpOffice
    ) {
        this.UserHttpOffice.getTeam().subscribe(
            data=> {
                console.log(data);
            },
            error => {

            }
        )
    }

    public ngOnInit() {
        this.route
            .data
            .subscribe((data: any) => {

                this.localState = data.yourData;
            });
        this.asyncDataWithWebpack();
    }
    private asyncDataWithWebpack() {
        setTimeout(() => {

            System.import('../../assets/mock-data/mock-data.json')
                .then((json) => {
                    console.log('async mockData', json);
                    this.localState = json;
                });

        });
    }

    public openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    public closeModal() {
        this.modalRef.hide();
        this.modalRef = null;
    }

    public deleteMember(index): void {
        this.memberList.splice(index, 1);
    }

    public getNotifyAddMember(event) {
        console.log(event);
        this.memberList.push(event);
    }

}
