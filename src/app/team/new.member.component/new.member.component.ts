import {Component, OnInit, Output, EventEmitter, TemplateRef, Input} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable } from "rxjs/Observable";
import { UserHttpOffice } from "../../service.project/private.ofice.service/user.http.ofice.service"
import {AbstractFormComponent} from "../../abstract.classes/abstract-form-component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { EMAIL_REGEXP } from  '../../utils/validation/validation.regexp.util';
import { emailErrorMessages } from '../../utils/validation/validation.messages.util';

@Component({
    moduleId: module.id,
    selector: 'new-member',
    templateUrl: 'new.member.component.html',
    providers: []
})

export class NewMemberComponent extends AbstractFormComponent implements OnInit {
    public modalRef: BsModalRef;
    public email: string;
    @Output() _notifyAdded: EventEmitter<any> = new EventEmitter();

    constructor( public modalService: BsModalService, private UserHttpOffice: UserHttpOffice, private formBuilder: FormBuilder) {
        super();
        /*this.meetingObjectSend.subscribe(() => {
         console.log(this.meetingObjectSend);
         })*/
    }

    public ngOnInit() {
        super.ngOnInit();
    }

    public addMember(): void {
        let nnn = {
            img: 'https://i.digiguide.tv/p/0809/tn-951-WillSmith-12203638190.jpg',
            name: 'Will Smith',
            email: 'test@test.com'
        };
        this._notifyAdded.emit(nnn);
        this.UserHttpOffice.setUser(this.form.value.email).subscribe(
            data=> {
                console.log(data);
            },
            error => {
                console.log(error);
            }
        )
    }

    protected buildForm(): FormGroup {
        return this.formBuilder.group({
            email: ['', {updateOn: 'blur', validators: [Validators.required, Validators.minLength(2), Validators.maxLength(255), Validators.pattern(EMAIL_REGEXP)]}],
        });
    }

    protected getValidationMessages(): {} {
        return ({
            email: emailErrorMessages,
        })
    }

    protected submit(): void {
        return this.addMember();
        //return this.login();
        //console.log(this.form.value.email, this.form.value.password);
    }

}


