import { TeamComponent } from './team.component'
import { NewMemberComponent } from './new.member.component/new.member.component'

export const TEAM_COMPONENT: any[] = [
    TeamComponent,
    NewMemberComponent,
];


