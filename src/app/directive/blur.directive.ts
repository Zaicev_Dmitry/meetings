import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
declare let $: any;
@Directive({
    selector: '[blur-directive]'
})
export class BlurDirective implements OnInit {
    @Input() focus: boolean;
    private element: HTMLElement;
    constructor ($element: ElementRef) {
        this.element = $element.nativeElement;
    }
    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        $(this.element).on('click', function () {
            $(this).blur();
            console.log(this)
        });
    }

}
