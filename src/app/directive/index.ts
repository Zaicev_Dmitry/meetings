import { NavCollapseDirective } from './nav.collapse.directive'
import { TimepickerInputClickDirective } from './timepicker.input.click.directive'
import {BlurDirective} from "./blur.directive";
import {RolesDirective} from "./roles.directive";

export const DIRECTIVES: any = [
    NavCollapseDirective,
    TimepickerInputClickDirective,
    BlurDirective
];
