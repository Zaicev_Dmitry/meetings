import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
declare let $: any;
@Directive({
    selector: '[nav-collapse]'
})
export class NavCollapseDirective implements OnInit {
    @Input() focus: boolean;
    private element: HTMLElement;
    private navbar_menu_visible: any;
    private list_route_link: any = [];
    constructor ($element: ElementRef) {
        this.element = $element.nativeElement;
    }

    ngOnInit(): void {
        let val = this;
        this.list_route_link = this.getListRouteLink();
        this.showCollapse();
        this.removeCollapse();
        this.initRightMenu();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if($(event.target).width() < 768){
         //   this.initRightMenu();
        }
    }

    public showCollapse(): void {
        let val = this;
        $('.navbar-collapse').on('shown.bs.collapse', function() {
            let $html = $('html');
            $html.addClass('nav-open');
        });
    }

    public initRightMenu(): void {
        let val = this;
        let content_buff;
        let $toggle = $('.navbar-toggle');
        let $a = $(this.element).find('.navbar-nav a');
        let closeMenu = function () {
            if(val.navbar_menu_visible == 1) {
                $('html').removeClass('nav-open');
                val.navbar_menu_visible = 0;
                $('#bodyClick').remove();
                setTimeout(function(){
                    $toggle.removeClass('toggled');
                }, 400);

            } else {
                setTimeout(function(){
                    $toggle.addClass('toggled');
                }, 430);

                let div = '<div id="bodyClick"></div>';
                $(div).appendTo("body").click(function() {
                    $('html').removeClass('nav-open');
                    $('.navbar-collapse').collapse('hide');
                    val.navbar_menu_visible = 0;
                    $('#bodyClick').remove();
                    setTimeout(function(){
                        $toggle.removeClass('toggled');
                    }, 400);
                });

                $('html').addClass('nav-open');
                val.navbar_menu_visible = 1;

            }
        };
        $toggle.click(function (){
            closeMenu();
        });
        $a.click(function (){
            closeMenu();
            $('.navbar-collapse').collapse('hide');
        });
    }

    public removeCollapse(): void {
        $('.navbar-collapse').on('hide.bs.collapse', function() {
            let $html = $('html');
            $html.removeClass('nav-open');
        });
    }

    public getListRouteLink(): any {
        let $navbar = $(this.element).find('.navbar-collapse').first();
        let linkList = $navbar.find('a');
        let arrNameLink = [linkList.length];
        [].forEach.call(linkList, function(element, i) {
            arrNameLink[i] = $(element)[0].attributes[1];
        });
        return arrNameLink;
    }

}