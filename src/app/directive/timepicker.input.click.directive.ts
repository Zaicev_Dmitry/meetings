import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
declare let $: any;
@Directive({
    selector: '[timepicker-input-click]'
})
export class TimepickerInputClickDirective implements OnInit {
    @Input() focus: boolean;
    private element: HTMLElement;
    constructor ($element: ElementRef) {
        this.element = $element.nativeElement;
    }
    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        let input = $(this.element).parent();
        $('time-picker').on('click', function () {
            $('timepicker').css('display', 'block');
        });
        $(document).mouseup(function (e) {
            let container = $('timepicker');
            if (container.has(e.target).length === 0){
               container.css('display', 'none');
            }
        });
    }

}