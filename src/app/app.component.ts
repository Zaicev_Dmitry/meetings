/**
 * Angular 2 decorators and services
 */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { environment } from 'environments/environment';
import { AppState } from './app.service';
import { NavigationEnd, Router, RouterState } from '@angular/router';
import { AuthService } from './service.project/auth.service';


declare var $: any;
/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  templateUrl: 'index.html'
})
export class AppComponent implements OnInit {
  public name = 'Angular Starter';
  public tipe = 'assets/img/tipe.png';
  public twitter = 'https://twitter.com/gdi2290';
  public url = 'https://tipe.io';
  public showDevModule: boolean = environment.showDevModule;
  private isFooter: boolean;
  private state: RouterState;
  public isAuth: boolean;

  constructor( public appState: AppState, private router: Router, public authService: AuthService ) {
    router.events.subscribe((val) => {
      if (!(val instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
      this.state = router.routerState;
      this.isFooter = (this.state.snapshot.url === '/') || (this.state.snapshot.url === '/home');
      this.isAuth = this.authService.isAuthenticated();
    });
  }

  public ngOnInit() {
    console.log('Initial App State', this.appState.state);
    this.isAuth = this.authService.isAuthenticated();
  }

  public logout() {
    this.authService.logout().subscribe(
        data => {
            localStorage.removeItem('currentUser');
            this.isAuth = this.authService.isAuthenticated();
            this.router.navigate(['/auth']);
        },
        err => {
            console.log('error');
            localStorage.removeItem('currentUser');
            this.isAuth = this.authService.isAuthenticated();
            this.router.navigate(['/auth']);
        }
    );
  }

}

/**
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
