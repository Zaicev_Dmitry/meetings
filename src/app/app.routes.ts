import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { RegistrationComponent } from './registration';
import { AuthComponent } from './authitification/authitification.component';
import { ScheduleComponent } from './schedule';
import { MeetingsComponent } from './meetings';
import { ProfileComponent } from './profile/profile.component';
import { NoContentComponent } from './no-content';
import { TeamComponent } from './team/team.component'
import { AdminComponent } from './admin/admin.component'
import { AuthGuardService } from "./service.project/auth.guard.service";

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'registration',  component: RegistrationComponent },
  { path: 'auth',  component: AuthComponent },
  { path: 'schedule',  component: ScheduleComponent, canActivate: [AuthGuardService] },
  { path: 'meetings',  component: MeetingsComponent, canActivate: [AuthGuardService]},
  { path: 'profile',  component: ProfileComponent, canActivate: [AuthGuardService] },
  { path: 'team',  component: TeamComponent, canActivate: [AuthGuardService] },
  { path: 'admin',  component: AdminComponent, canActivate: [AuthGuardService] },
  { path: 'about', component: AboutComponent, canActivate: [AuthGuardService] },
  { path: 'detail', loadChildren: './+detail#DetailModule'},
  { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
  { path: '**',    component: NoContentComponent },
];
