export class Meeting {
    private _title: string;
    private _location: string;
    private _day: Date;
    private _teem: any;
    private _duration: string;

    public getTitle(): string {
        return this._title;
    }

    public setTitle(value: string) {
        this._title = value;
    }

    public getLocation(): string {
        return this._location;
    }

    public setLocation(value: string) {
        this._location = value;
    }

    public getDay(): Date {
        return this._day;
    }

    public setDay(value: Date) {
        this._day = value;
    }

    public getTeem(): any {
        return this._teem;
    }

    public setTeem(value: any) {
        this._teem = value;
    }

    public getDuration(): string {
        return this._duration;
    }

    public setDuration(value: string) {
        this._duration = value;
    }
}