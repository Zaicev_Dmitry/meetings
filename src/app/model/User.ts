import {initTransferState} from "@angular/platform-browser/src/browser/transfer_state";
export class User {
    private _id: number;
    private _first_name: string;
    private _surname: string;
    private _company: string;
    private _email: string;
    private _password: any;
    private _status: string;

    public getId(): number {
        return this._id;
    }

    public setId(id): void {
        this._id = id;
    }

    public getFirst_name(): string {
        return this._first_name;
    }

    public getSurname(): string {
        return this._surname;
    }

    public getCompany(): string {
        return this._company;
    }

    public getEmail(): string {
        return this._email;
    }

    public getPassword(): any {
        return this._password;
    }

    public getStatus(): string {
        return this._status;
    }

    public setStatus(value: string) {
        this._status = value;
    }
}
 export enum Status {
    ACTIVE = 'active',
    NOT_ACTIVE = 'not_active',
    DELETED = 'deleted'
 };

export interface IUser {
    id: number;
    team_id: number;
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    organization: string;
    image: string;
    status: string;
    created_at: string;
    deleted_at: string;
}
