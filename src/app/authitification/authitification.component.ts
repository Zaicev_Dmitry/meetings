import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractFormComponent } from '../abstract.classes/abstract-form-component';
import { emailErrorMessages } from '../utils/validation/validation.messages.util';
import { EMAIL_REGEXP } from  '../utils/validation/validation.regexp.util';
import { passwordErrorMessages } from '../utils/validation/validation.messages.util';
import {ActivatedRoute, Router} from '@angular/router';
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { AuthService } from "../service.project/auth.service";
import { GOOGLE_KEY, GOOGLE_SCOPES, MICROSOFT_APP_ID, AUTH_ENDPOINT_MICROSOFT, REDIRECT_URI, MICROSOFT_SCOPES } from "../utils/config/config"
declare const gapi: any;
declare let $: any;

@Component({
    moduleId: module.id,
    selector: 'auth',
    templateUrl: 'authitification.component.html',
    providers: []
})
export class AuthComponent extends AbstractFormComponent implements OnInit {

    failed: boolean;
    loading: boolean;
    errorMessage: string;
    email: string;
    isOffFocusEmail: boolean;
    isOffFocusPass: boolean;
    resolvedData: any;
    public localState: any;
    public modalRef: BsModalRef;
    public auth2: any;

    constructor( public route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, public modalService: BsModalService, public authService: AuthService) {
        super();
        this.loading = false;
        this.failed = false;
    }

    public ngOnInit() {
        super.ngOnInit();
        this.route
            .data
            .subscribe((data: any) => {

                this.localState = data.yourData;
            });

        console.log('hello `Registratration` component');

        this.asyncDataWithWebpack();
    }
    private asyncDataWithWebpack() {
        setTimeout(() => {

            System.import('../../assets/mock-data/mock-data.json')
                .then((json) => {
                    console.log('async mockData', json);
                    this.localState = json;
                });

        });
    }

    protected buildForm(): FormGroup {
        return this.formBuilder.group({
            email: ['', {updateOn: 'blur', validators: [Validators.required, Validators.minLength(2), Validators.maxLength(255), Validators.pattern(EMAIL_REGEXP)]}],
            password: ['', {updateOn: 'blur', validators: [Validators.required, Validators.minLength(6), Validators.maxLength(255)]}]
        });
    }

    protected submit(): void {
        return this.login();
        //console.log(this.form.value.email, this.form.value.password);
    }


    protected getValidationMessages(): {} {
        return ({
            email: emailErrorMessages,
            password: passwordErrorMessages
        })
    }

    public openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    public closeModal() {
        this.modalRef.hide();
        this.modalRef = null;
    }

    public login() {
        this.authService.login(this.form.value.email, this.form.value.password).subscribe(
            data=> {
                this.router.navigate(['/schedule']);
            },
            error => {
                console.log("no user");
            }
        )
    }

    public googleInit() {
        gapi.load('auth2', () => {
            this.auth2 = gapi.auth2.init({
                client_id: GOOGLE_KEY
            });
            this.attachSignin(document.getElementById('googleBtn'));
        });
    }

    public attachSignin(element) {
        this.auth2.attachClickHandler(element, {},
            (googleUser) => {

                let profile = googleUser.getBasicProfile();
            }, (error) => {
                alert(JSON.stringify(error, undefined, 2));
            });
    }

    public buildAuthUrlMicrosoft() {
        let authParams = {
            response_type: 'id_token token',
            client_id: MICROSOFT_APP_ID,
            redirect_uri: REDIRECT_URI,
            scope: MICROSOFT_SCOPES,
            state: '',
            nonce: '',
            response_mode: 'fragment'
        };
        return authParams + $.param(authParams);
    }

    ngAfterViewInit(){
        this.googleInit();
    }
}

